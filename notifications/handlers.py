from functools import wraps
from typing import Union, Callable

import boto3
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.utils import timezone
from twilio.rest import Client as TwilioClient

from notifications import models as notification_models
from notifications.notifications_config import config
from notifications.utils import (
    get_type_extras_from_notification_type,
    import_from_string,
)


def notification_handler(
    fn: 'Callable[[Union[int, str, notification_models.Notification]], None]',
) -> 'Callable[[Union[int, str, notification_models.Notification]], None]':
    """
    Notification handler decorator.
    Decorated function should send notification.
    It handles fetching `Notification` instance if pk is provided instead it,
    and sets `DONE` status if function didn't raise or `ERROR` status
    if notification raised. Exception is raised further after status is
    saved in database.
    """
    @wraps(fn)
    def wrapper(
        notification: 'Union[notification_models.Notification, str, int]') -> 'None':
        # This function can be called as celery task.
        # If this function is run as celery task, arguments are serialized and we
        # cannot serialize instance so we pass pk to notification instance and
        # then we fetch it from db.
        if not isinstance(notification, notification_models.Notification):
            notification = notification_models.Notification.objects.get(
                pk=notification)

        try:
            fn(notification)
        except Exception as exc:
            notification.set_status(notification.Status.ERROR)
            raise exc
        else:
            notification.set_status(notification.Status.DONE)

    return wrapper


@notification_handler
def email_handler(
    notification: 'Union[notification_models.Notification, str, int]'
) -> 'None':
    type_extras = get_type_extras_from_notification_type(notification.type)
    template_txt = type_extras['template_txt']
    template_html = type_extras['template_html']
    subject = type_extras['subject']

    body_txt = get_template(template_txt).render(notification.context)
    body_html = get_template(template_html).render(notification.context)
    email = EmailMultiAlternatives(subject, body_txt,
                                   settings.DEFAULT_FROM_EMAIL,
                                   notification.recipients)
    email.attach_alternative(body_html, 'text/html')
    email.send()


@notification_handler
def ses_email_handler(
    notification: 'Union[notification_models.Notification, str, int]'
) -> 'None':
    email_sender = '{} <{}>'.format(settings.EMAIL_HOST_USER,
                                    settings.DEFAULT_FROM_EMAIL)

    type_extras = get_type_extras_from_notification_type(notification.type)
    template_txt = type_extras['template_txt']
    template_html = type_extras['template_html']
    subject = type_extras['subject']

    body_txt = get_template(template_txt).render(notification.context)
    body_html = get_template(template_html).render(notification.context)

    ses = boto3.client(
        'ses',
        aws_access_key_id=settings.AWS_SES_ACCESS_KEY_ID,
        aws_secret_access_key=settings.AWS_SES_SECRET_ACCESS_KEY,
        region_name=settings.AWS_SES_REGION_NAME)

    ses.send_email(
        Source=email_sender,
        Destination={
            'ToAddresses': notification.recipients,
        },
        Message={
            'Subject': {
                'Data': subject,
                'Charset': 'utf-8',
            },
            'Body': {
                'Text': {
                    'Data': body_txt,
                    'Charset': 'utf-8',
                },
                'Html': {
                    'Data': body_html,
                    'Charset': 'utf-8',
                }
            }
        }
    )


@notification_handler
def twilio_sms_handler(
    notification: 'Union[notification_models.Notification, str, int]'
) -> 'None':
    twilio = TwilioClient(settings.TWILIO_ACCOUNT_SID,
                          settings.TWILIO_AUTH_TOKEN)
    type_extras = get_type_extras_from_notification_type(notification.type)
    template_txt = type_extras['template_txt']
    body = get_template(template_txt).render(notification.context)

    for recipient in notification.recipients:
        twilio.api.account.messages.create(
            to=recipient,
            from_=settings.TWILIO_FROM_PHONE_NUMBER,
            body=body,
        )


# We check if configuration provides celery.
# and if so define `celery_email_handler` which is celery task.
# Note that this file need to be run at application startup to
# register this task. This is done by "ready()" hook in `notifications.apps` module.
if config.get('CELERY', None) is not None:
    celery_app = import_from_string(config['CELERY'])

    celery_email_handler_task = celery_app.task(email_handler)
    celery_ses_email_handler_task = celery_app.task(ses_email_handler)
    celery_twilio_sms_handler_task = celery_app.task(twilio_sms_handler)


    def celery_email_handler(
        notification: 'notification_models.Notification'
    ) -> 'None':
        celery_email_handler_task.apply_async(
            (notification.pk,),
            eta=timezone.now() + timezone.timedelta(seconds=1))


    def celery_ses_email_handler(
        notification: 'notification_models.Notification'
    ) -> 'None':
        celery_ses_email_handler_task.apply_async(
            (notification.pk,),
            eta=timezone.now() + timezone.timedelta(seconds=1))


    def celery_twilio_sms_handler(
        notification: 'notification_models.Notification'
    ) -> 'None':
        celery_twilio_sms_handler_task.apply_async(
            (notification.pk,),
            eta=timezone.now() + timezone.timedelta(seconds=1))

else:
    celery_email_handler = None
    celery_ses_email_handler = None
    celery_twilio_sms_handler = None
