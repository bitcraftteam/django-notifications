from functools import reduce

from django.conf import settings

config = dict(settings.NOTIFICATION_CONFIG)

# Creates list [(choice_value, choice_name), ...)]
# where choice value is saved in database
# and choice name is "human readable" value.
config['TYPE_CHOICES'] = reduce(
    lambda accumulator, item: accumulator + [item['choice_name']],
    config['TYPES'],
    []
)
