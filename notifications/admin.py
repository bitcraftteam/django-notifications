from django.contrib import admin

from notifications.models.notification import Notification


def dispatch_notifications(modeladmin, request, queryset):
    """
    Django admin action.
    Iterates over provided queryset[Notification] and calls dispatch
    on every instance.
    """
    notification: 'Notification'
    for notification in queryset:
        notification.dispatch()


dispatch_notifications.short_description = 'Dispatch a notification.'


@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    list_display = ['id', 'status', 'type', 'recipients', 'language', 'created']
    list_filter = ['status', 'type', 'language']
    search_fields = ['status', 'type', 'recipients']
    actions = [dispatch_notifications]
