from typing import Union

from django.apps import apps


def django_model_matcher(
    *args,
    model: 'Union[str, None]' = None,
    **kwargs
) -> bool:
    """
    Checks if first arg is instance of a model.
    """
    if model is None:
        return False
    if not args:
        return False
    instance = args[0]
    app_label, model_name = model.split('.')
    model = apps.get_model(app_label=app_label, model_name=model_name)
    return isinstance(instance, model)
