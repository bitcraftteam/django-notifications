import importlib


def import_from_string(import_string):
    """
    Imports class or function based on import string provided as parameter.

    :param function_string: :class:`python:str` or :func:`python:unicode`
        String with class or function name, e.g. 'module.function'
    :return: Class or function specified in parameter
    """
    mod_name, import_name = import_string.rsplit('.', 1)
    mod = importlib.import_module(mod_name)
    return getattr(mod, import_name)


def get_type_extras_from_notification_type(notification_type) -> dict:
    from notifications.models.notification import config

    for value in config['TYPES']:
        if notification_type == value['choice_name'][0]:
            return value

    raise Exception(f'No type_extras found for type {type}')
