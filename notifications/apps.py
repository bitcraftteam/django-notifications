from django.apps import AppConfig


class NotificationsConfig(AppConfig):
    name = 'notifications'

    def ready(self):
        """
        Imports handlers file to register
        celery tasks (task is registered only if `settings.NOTIFICATION_CONFIG.CELERY != None`).
        Note that ``default_app_config = 'notifications.apps.NotificationsConfig'``
        line is added in `__init__.py` of this module to run this hook.
        """
        # noinspection PyUnresolvedReferences
        from notifications import handlers
