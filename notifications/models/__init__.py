from notifications.models.notification import Notification

__all__ = [
    'Notification',
]