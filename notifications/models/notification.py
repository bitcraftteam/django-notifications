from enum import Enum
from functools import partial
from types import FunctionType
from typing import Callable
from typing import Union

from django.conf import settings
from django.db import models
from jsonfield import JSONField

from notifications.matchers import django_model_matcher
from notifications.notifications_config import config
from notifications.utils import (
    import_from_string,
    get_type_extras_from_notification_type
)


class NotificationManager(models.Manager):
    def create(self, *args, dispatch_now=False, **kwargs):
        """
        This method has different behaviour than default one.
        Passed `args` and `kwargs` are passed further
        to match functions. First match function that will
        return `True` with provided `args` and `kwargs`
        will indicate which `parser_function` to use.
        Then `parser_function` receives the same args, kwargs
        and in addition named argument `type_extras`.
        `parser_function` should return kwargs (dict)
        which will be used to create an instance.

        Parameter `dispatch_now` is optional and is not passed
        to `match_function` nor to `parser_function`.
        If `dispatch_now` is `True` then `.dispatch()` method
        is called immediately after instance is saved.
        """
        parser_function = None
        type_extras = None

        # Extract `match_function`, then check if provided
        # to this method match and if so, extract `parser_function`.
        for value in config['TYPES']:
            match_function: 'Union[str, FunctionType]' = value['match_function']

            # Check for included match function
            if match_function == 'django_model':
                model = value['model']
                match_function = partial(django_model_matcher, model=model)

            # Import match function from string
            elif not isinstance(match_function, FunctionType):
                match_function = import_from_string(match_function)

            if match_function(*args, **kwargs):
                parser_function: 'Union[str, FunctionType]' = value['parser_function']
                if not isinstance(parser_function, FunctionType):
                    parser_function = import_from_string(parser_function)

                type_extras = value
                break

        if not parser_function:
            raise Exception('No parser function found.')

        # Then pass, *args, **kwargs and type_extras to the parser_function.
        # type_extras is full config of this given type.
        # Those are: choice_name, match_function, parser_function...
        # and everything we provided in this type.
        parsed_data = parser_function(*args, type_extras=type_extras, **kwargs)

        notification_type = type_extras['choice_name'][0]
        obj = self.model(type=notification_type, **parsed_data)
        self._for_write = True
        obj.save(force_insert=True, using=self.db, dispatch_now=dispatch_now)
        return obj


class Notification(models.Model):
    class Status(Enum):
        @classmethod
        def choices(cls):
            return tuple((name, member.value)
                         for name, member in cls.__members__.items())

        NEW = 'NEW'
        PENDING = 'PENDING'
        DONE = 'DONE'
        ERROR = 'ERROR'

    type = models.CharField(
        max_length=255,
        choices=config['TYPE_CHOICES'],
    )
    status = models.CharField(
        max_length=255,
        choices=Status.choices(),
        default=Status.NEW.value,
    )
    created = models.DateTimeField(auto_now_add=True)
    context = JSONField(blank=True)
    recipients = JSONField()
    extras = JSONField(null=True, blank=True)

    language = models.CharField(
        max_length=32,
        choices=settings.LANGUAGES,
        default=settings.LANGUAGE_CODE
    )

    objects = NotificationManager()

    def __str__(self):
        return f'[{self.status}] {self.type} - {self.recipients}'

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None, dispatch_now=False):
        """
        Saves instance in default way.
        The only difference is parameter `dispatch_now` which when set to `True`
        calls `dispatch()` method automatically after save.
        """
        super(Notification, self).save(force_insert=force_insert,
                                       force_update=force_update,
                                       using=using,
                                       update_fields=update_fields)
        if dispatch_now:
            self.dispatch()

    def dispatch(self) -> 'None':
        """
        Sends the notification associated with this instance.
        """
        if (hasattr(settings, 'NOTIFICATIONS_DUMMY_HANDLER')
                and settings.NOTIFICATIONS_DUMMY_HANDLER):
            self.set_status(self.Status.DONE)
            return

        handler = self.get_handler()
        handler(self)

    def get_handler(self) -> 'Callable[[Notification], None]':
        """
        Search for dispatch handler which is in charge
        of sending notification.
        Handler should be specified in task config as value of
        key `handler`.
        Returned handler should be callable object (e.g. function).
        """
        type_extras = get_type_extras_from_notification_type(self.type)
        handler = type_extras['handler']

        if handler == 'email':
            from notifications.handlers import email_handler
            return email_handler
        elif handler == 'celery_email':
            from notifications.handlers import celery_email_handler
            return celery_email_handler
        elif handler == 'email:ses':
            from notifications.handlers import ses_email_handler
            return ses_email_handler
        elif handler == 'celery_email:ses':
            from notifications.handlers import celery_ses_email_handler
            return celery_ses_email_handler
        elif handler == 'sms:twilio':
            from notifications.handlers import twilio_sms_handler
            return twilio_sms_handler
        elif handler == 'celery_sms:twilio':
            from notifications.handlers import celery_twilio_sms_handler
            return celery_twilio_sms_handler

        if not isinstance(handler, FunctionType):
            handler = import_from_string(handler)

        return handler

    def set_status(self, status: 'Notification.Status'):
        self.status = Notification.Status(status).value
        self.save(dispatch_now=False)
