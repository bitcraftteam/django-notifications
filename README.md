# Django notifications

Django app to help managing and handling email and sms notifications.
All notifications are saved in database with status (NEW, PENDING, etc.) and option to dispatch (resend) them.


To install app add following add following code to `INSTALLED_APPS`:

    INSTALLED_APPS = [
        ...
        'notifications',
        ...
    ]

And add custom path to migrations for this app:

    MIGRATION_MODULES = {
        'notifications': 'notifications_migrations',
    }


Now, you define notification config in `settings.py`.
Note that default handlers (they are described below in this document)
requires additional settings to be provided.

**Note**: You should to run `python manage.py makemigrations` and `python manage.py migrate`
each time you add, remove or change name of the "type".

Creation of the notification is done by `Notficaiotn.objects.create()`
method. It works like this:

- we pass args and kwargs to `.create()` method.
- it iterates over `TYPES` in `NOTIFICATION_CONFIG` and calls every `match_function` in it until one of them return `True`
- args and kawrgs are passed to `parser_function` specified in matched type
- `parser_function` returns kwargs to create Notification instance

`.create()` also accepts `dispatch` argument which if set to `True`
dispatches notification immediately after instance is saved in database.
`Notification` model also has `.dispatch()` method which sends a notification.
It is done by handler function specified in type. This function can receive
`Notification` instance or it's pk.

Example config:

    NOTIFICATION_CONFIG = {

        # Optional but needed if 'celery_email' handler is used.
        # It should be import path to the celery instance.
        'CELERY': 'apicraft.celery.app',

        'TYPES': [
            {
                'choice_name': ('registration_email', 'Registration Email'),
                # Function or import string to the function
                # that will decide whether provided data
                # should be handled by this type.
                'match_function': 'core.emails.match_register_email',

                # Function or import string to the function
                # that will parse provided data
                # and return kwargs to create `Notification`
                # model instance.'
                # It should provide following keys:
                #   - context
                #   - recipients
                #   - extras
                #   - language
                #
                # extras and language is optional.
                'parser_function': 'core.emails.parse_register_email',

                # String to predefined handler or function or
                # import string to the function that will
                # handle sending notification.
                # 'handler': 'celery_email',
                'handler': 'celery_email',

                # Additional data for handler.
                # subject, template_txt and template_html are
                # used by 'email' (predefined) handler.
                'subject': ugettext_lazy('APICraft email verification'),
                'template_txt': 'core/emails/register.txt',
                'template_html': 'core/emails/register.html',
            },
            {
                'choice_name': ('reset_password', 'Reset password'),
                'match_function': 'core.emails.match_reset_password',
                'parser_function': 'core.emails.parse_reset_password',
                'handler': 'celery_email',
                'subject': ugettext_lazy('APICraft password restoration'),
                'template_txt': 'core/emails/reset-password.txt',
                'template_html': 'core/emails/reset-password.html',
            }
        ],
    }


Example of usage:


    from notifications.models import Notification
    from core.models import UserActivation

    u = UserActivation.objects.first()
    Notification.objects.create(u, dispatch_now=True)


where match function is defined as follow:

    def match_register_email(*args, **kwargs) -> 'bool':
        """
        If first args is instance of `UserActivation` model then
        return `True`, `False` otherwise.
        """
        if not args:
            return False
        user_activation = args[0]
        return isinstance(user_activation, core_models.UserActivation)

parser function:

    def parse_register_email(user_activation: 'core_models.UserActivation', type_extras: 'dict') -> 'dict':
        """
        Maps `UserActivation` instance to kwargs for new instance of `Notification`.
        """
        link = '{}/{}'.format(settings.FRONTEND_VERIFICATION_ADDRESS.rstrip('/'), user_activation.key)
        context = {
            'username': user_activation.user.username,
            'link': link,
        }
        recipients = [user_activation.user.email]
        return {
            'context': context,
            'recipients': recipients,
        }


### Default handler functions:

To enable `celery_` option pass string import path to celery instance in settings.

    NOTIFICATION_CONFIG = {
        'CELERY': '<your_project>.celery.app',
        ...
    }



**`email`, `celery_email`**:

Sends email in "django default" way.

Required settings:

    EMAIL_USE_TLS = os.environ.get('EMAIL_USE_TLS').lower().strip() != 'false'
    EMAIL_HOST = os.environ['EMAIL_HOST']
    EMAIL_PORT = int(os.environ['EMAIL_PORT'])
    EMAIL_HOST_USER = os.environ['EMAIL_HOST_USER']
    EMAIL_HOST_PASSWORD = os.environ['EMAIL_HOST_PASSWORD']
    DEFAULT_FROM_EMAIL = os.environ['DEFAULT_FROM_EMAIL']

Required additional type options:

    {
        ...
        'handler': 'celery_email',
        'subject': ugettext_lazy('Email title'),
        'template_txt': 'core/emails/reset-password.txt',
        'template_html': 'core/emails/reset-password.html',
    }


**`email:ses`, `celery_email:ses`**:

Sends email using AWS SES service. Required settings:

    EMAIL_HOST_USER = os.environ['EMAIL_HOST_USER']
    DEFAULT_FROM_EMAIL = os.environ['DEFAULT_FROM_EMAIL']
    AWS_SES_ACCESS_KEY_ID = os.environ['AWS_SES_ACCESS_KEY_ID']
    AWS_SES_SECRET_ACCESS_KEY = os.environ['AWS_SES_SECRET_ACCESS_KEY']
    AWS_SES_REGION_NAME = os.environ['AWS_SES_REGION_NAME']

Required additional type options:

    {
        ...
        'handler': 'celery_email:ses',
        'subject': ugettext_lazy('Email title'),
        'template_txt': 'core/emails/reset-password.txt',
        'template_html': 'core/emails/reset-password.html',
    }


**`sms:twilio`, `celery_sms:tiliio`**

Sends sms using Twillio. Required settings:

    TWILIO_ACCOUNT_SID = os.environ['TWILIO_ACCOUNT_SID']
    TWILIO_AUTH_TOKEN = os.environ['TWILIO_AUTH_TOKEN']
    TWILIO_FROM_PHONE_NUMBER = os.environ['TWILIO_FROM_PHONE_NUMBER']

Required additional type options:

    {
        ...
        'handler': 'celery_sms:twilio',
        'template_txt': 'core/sms/reminder.txt',
    }


### Included match functions

**`django_model`**:

Check if first argument provided to `.create()` method is a specified model.
Required type option: `model` - app model string represented as `<app_label>.<model_name>`.

Example:

    {
        ...
        'match_function': 'django_model',
        'model': 'app.User',
    }


### Disabling notification

You can disable sending messages (for test purposes) by setting `NOTIFICATIONS_DUMMY_HANDLER = True` in `settings.py`.
It's done at `Notification.dispatch()` method and will set notification status to `DONE` immediately
without calling proper handler function.
